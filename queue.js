let collection = [];

// Write the queue functions below.

function print() {
    return collection
}

function enqueue(data) {

    collection[collection.length] = data  

    return collection
}

function dequeue() {
    let newCollection = []

    for (let i = 0; i < collection.length; i++) {
        if(i != 0) {
            newCollection[i-1] = collection[i] 
        }
    }
    collection = newCollection

    return collection
}

function front() {
    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
    return collection.length ? false : true
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};